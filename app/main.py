from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from app.routes import routes
import app.config as config

application = FastAPI()
application.debug = config.DEBUG

templates = Jinja2Templates(directory='templates')
application.mount('/static', StaticFiles(directory='statics'), name='static')


application.mount('/', routes)
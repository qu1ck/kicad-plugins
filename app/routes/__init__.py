from starlette.routing import Mount, Route, Router
from fastapi.responses import RedirectResponse
from urllib.parse import urljoin

from app.config import BASE_PCM_SOURCE
from .api import api_routes

async def repojson(request):
    url = urljoin( BASE_PCM_SOURCE, "repository.json" )
    return RedirectResponse(url, status_code=307)
    
async def packagesjson(request):
    url = urljoin( BASE_PCM_SOURCE, "packages.json" )
    return RedirectResponse(url, status_code=307)

# mounted app can also be an instance of `Router()`
routes = Router([
    Route("/repository.json", endpoint=repojson),
    Route("/packages.json", endpoint=packagesjson),
    Mount('/api', app=api_routes),
])
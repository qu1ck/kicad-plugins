from fastapi import FastAPI
from starlette.routing import Mount, Route, Router
from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse, \
    StreamingResponse, \
    FileResponse, \
    HTMLResponse

from .v1_routes import app as v1_routes

api_routes = FastAPI()
api_routes.mount('/v1/', app=v1_routes)
from fastapi import FastAPI
from fastapi.responses import JSONResponse, \
    PlainTextResponse, \
    RedirectResponse, \
    StreamingResponse, \
    FileResponse, \
    HTMLResponse

from app.models import *

app = FastAPI()

@app.route('/')
async def index(request):
    content = []
    return JSONResponse(content)


@app.route('/stats/ratings')
async def get_ratings(request):
    content = []
    return JSONResponse(content)


@app.route('/stats/rating/{guid}', methods=["POST"])
async def post_rating(request):
    guid = request.path_params['guid']
    content = { "guid": guid }
    return JSONResponse(content)

    
@app.route('/stats/download/{guid}', methods=["POST"])
async def post_download(request, guid):
    guid = request.path_params['guid']
    content = { "guid": guid }
    return JSONResponse(content)
from starlette.config import Config
from starlette.datastructures import URL, Secret

# Config will be read from environment variables and/or ".env" files.
_config = Config(".env")

DEBUG = _config('DEBUG', cast=bool, default=False)
TESTING = _config('TESTING', cast=bool, default=False)
DATABASE_URL = _config('DATABASE_URL', default='')

BASE_PCM_SOURCE = _config('BASE_PCM_SOURCE', default='https://gitlab.com/qu1ck/kicad-pcm-repository/-/raw/master/')